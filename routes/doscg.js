require('dotenv').config()
var express = require('express');
var router = express.Router();
const Parser = require('expr-eval').Parser;
var mcache = require('memory-cache');

var cache = (duration) => {
    return (req, res, next) => {
      let key = '__express__' + req.originalUrl || req.url
      let cachedBody = mcache.get(key)
      if (cachedBody) {
        return
      } else {
        res.sendResponse = res.send
        res.send = (body) => {
          mcache.put(key, body, duration * 1000);
          res.sendResponse(body)
        }
        next()
      }
    }
  }

/* GET Answer for question1. */
router.get('/question1', cache(10), function(req, res, next) {
    var parser = new Parser();
    let x = Symbol('x')
    let y = Symbol('y')
    let z = Symbol('z')
    let nv_process = parser.parse('cv-((2*ca)-2)')
    let cv_process = parser.parse('nv+(2*(ca-1))')
    let cv_process_back = parser.parse('pv-(2*ca)')

    var list = [x,y,5,9,15,23,z]
    var result  = [0,0,0,0,0,0,0]
    for (i = (list.length-1); i > 0; i--) {
        if(typeof list[i] === "symbol")
        {
            if(typeof list[i-1] === "number")
                result[i] = parseInt(cv_process.evaluate({ ca: i,nv:list[i-1]}))
            else
                result[i] = parseInt(cv_process_back.evaluate({ ca: i,pv:list[i+1]}))
        }
        result[i-1] = nv_process.evaluate({ ca: i,cv:result[i]})
    }
    res.send({question:'X, Y, 5, 9, 15, 23, Z - Please create a new function for finding X, Y, Z value?',X:'X='+result[0],Y:'Y='+result[1],Z:'Z='+result[6]});
});


/* GET Answer for question2. */
router.get('/question2', cache(10), function(req, res, next) {
    var parser = new Parser();
    var a = 21;
    var b = parser.parse('23-a');
    var c = parser.parse('-21-a');
    res.send({question:'If A = 21, A + B = 23, A + C = -21 - Please create a new function for finding B and C value?',A:'A = '+a, B:'B = '+ b.evaluate({ a: 21 }) ,C:'C = '+c.evaluate({ a: 21 })});
});

/* GET Answer for question3. */
router.get('/question3',cache(10), function(req, res, next) {
    // setTimeout(() => {
        const {Client} = require("@googlemaps/google-maps-services-js");
        const client = new Client({});
        client
        .directions({
            params: {
                origin:'SCG สำนักงานใหญ่',
                destination:'เซ็นทรัลเวิลด์',
                key: process.env.GOOGLE_API_KEY,
                mode:'driving',
                language :'th'
            },
            timeout: 1000, // milliseconds
        })
        .then((r) => {
            res.send({question:'Please use “Google API” for finding the best way to go to Central World from SCG Bangsue?',data:r.data})
        })
        .catch((e) => {
            res.send(e);
        });
    // }, 3000) //setTimeout was used to simulate a slow processing request
});



module.exports = router;
